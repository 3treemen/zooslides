<?php
/**
 * Plugin Name: Zoo Slides
 * Plugin URI: https://zoo.nl
 * Description: A slider as basic as possible
 * Version: 0.1
 * Author: Ronnie Stevens
 * Licence: GPL v2 or later
 * Text Domain: zoo-slides
 */


defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/* initiate a custom post type for the slides */
function zooslides_init() {
    $args = array(
        'label' => "Zoo slides",
        'public' => true,
        'supports' => array(
            'title',
            'thumbnail',
            'excerpt',
        )
        );
        register_post_type( 'zooslides', $args );
        add_shortcode('zooslides', 'zooslides_function');
}
add_action( 'init' , 'zooslides_init' );

/* Registering and Including Scripts and Styles */
add_action('wp_enqueue_scripts', 'zooslides_script');
function zooslides_script() {
    if (!is_admin()) {
        wp_enqueue_script('zooslides_script',
                           plugins_url( 'zooslides.js', __FILE__),
                           array('jquery'));
    }
}
 
function zooslides_register_styles() {
    // register
    wp_register_style('zooslides_styles', plugins_url('zooslides.css', __FILE__));
 
    // enqueue
    wp_enqueue_style('zooslides_styles');
}
add_action('wp_print_styles', 'zooslides_register_styles');

/* add image size */
add_image_size('zooslides_size', 600, 280, true);
add_theme_support( 'post-thumbnails' );

/* query the slides and generate html */
function zooslides_function($type='zooslides_size') {
    $args = array(
        'post_type' => 'zooslides',
        'posts_per_page' => 5
    );
    $result = '<div id="slideshow">';
 
    //the loop
    $loop = new WP_Query($args);
    if ($loop->have_posts() ) : 
        while ( $loop->have_posts() ) : $loop->the_post(); 
            // Display post content
            $title = get_the_title();
            $excerpt = get_the_excerpt();
            $the_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
            $result .='<div style="background-image:url('.$the_url .');">';
            $result .='<div class="slidetext"><h2>' .$title .'</h2><span>' . $excerpt .'</span></div>';
            $result .='</div>';
        endwhile; 
    endif; 

    wp_reset_query(); 

    // if ($loop->have_posts()){
    //     while ($loop->have_posts()) {
    //     }
    // }

    $result .='</div>';
    return $result;
    }