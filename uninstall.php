<?php

// if uninstall.php is not called by WordPress, die
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}
function delete_post_type($post_type = 'zooslides'){
       unregister_post_type( $post_type );
   }
add_action('init','delete_post_type');